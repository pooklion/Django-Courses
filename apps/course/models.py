
from __future__ import unicode_literals

from django.db import models

class Course(models.Model):
    name = models.CharField(max_length = 255)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

class Desc(models.Model):
    description = models.TextField()
    desc_of = models.OneToOneField(Course, related_name = 'course_desc')
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

class Comment(models.Model):
    comment = models.TextField()
    comment_of = models.ForeignKey(Course, related_name = 'course_comments')
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
