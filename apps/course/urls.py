from django.conf.urls import url 
from . import views

urlpatterns = [
    url(r'^$', views.index),
    url(r'^add_course$', views.add_course),    
    url(r'^delete_course/(?P<course_id>\d+)$', views.delete_course),
    url(r'^delete/(?P<course_id>\d+)$', views.delete),
    url(r'^comment_course/(?P<course_id>\d+)$', views.comment_course),
    url(r'^add_comment/(?P<course_id>\d+)$', views.add_comment),
]