from __future__ import unicode_literals
from django.shortcuts import render, redirect
from django.contrib import messages
from models import *

def index(request):
    context = {
        'course' : Course.objects.all().order_by('name')
    }
    return render(request, 'course/index.html', context)

def add_course(request):
    error = False
    if len(request.POST['name']) < 6:
        error = True
        messages.error(request, 'Course Name required more than 5 characters.')
    if len(request.POST['desc']) < 16:
        error = True
        messages.error(request, 'Course Description required more than 15 characters.')
    if error:
        return redirect('/')
    else:
        new_course = Course(name = request.POST['name'])
        new_course.save()
        desc = Desc(desc_of = new_course , description = request.POST['desc'])
        desc.save()
    return redirect('/')

def delete_course(request, course_id):
    context = {
        'course' : Course.objects.get(id=course_id)
    }
    return render(request, 'course/delete.html', context)

def delete(request, course_id):
    Course.objects.get(id = course_id).delete()
    return redirect('/')

def comment_course(request, course_id):
    context = {
        'course' : Course.objects.get(id = course_id),
        'comments' : Comment.objects.filter(comment_of__id = course_id)
    }
    return render(request, 'course/add_comment.html', context)

def add_comment(request, course_id):
    course = Course.objects.get(id = course_id)
    Comment.objects.create(comment = request.POST['comment'], comment_of = course)
    return redirect('/comment_course/'+str(course_id))