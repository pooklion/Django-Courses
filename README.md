# Django Assigment - Courses

Make a new Django project and application, and complete the functionality of the wireframe below.  Require the course name to be more than 5 characters and the description to be more than 15 characters.

Bonus Features (Optional)
Make description a one-to-one relationship with the Course table rather than a column in the course table (intermediate).
Add a <button> that allows you to add comments about the courses and render a page with those comments (advanced)

![](http://s3.amazonaws.com/General_V88/boomyeah/company_209/chapter_3834/handouts/chapter3834_6619_mvc-courses.png)
